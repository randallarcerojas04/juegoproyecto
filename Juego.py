import sys
import random as rd
listaV = []
matriz1 = [[0 for i in range(5)] for j in range(5)]
matriz2 = [[0 for o in range(5)] for h in range(5)]
lista1 = [1, 2, 3]
lista2 = [1, 2, 3]
cantidadNaves= 0
conta=0
contb=0

class Vehiculo:
    '''Es una clase, se encarga de los atributos del metodo agregarVehiculos'''
    def __init__(self, Nombre, Armas):
        self.nombre = Nombre
        self.armas = Armas


def estadisticas():
    '''Muestra la cantidad de naves con la que inicio y la catidad de naves que derrió.'''
    print("Jugador 1 inició con: "+str(cantidadNaves)+" naves")
    print("Jugador 1 derribó: " + str(cantidadNaves - (lista1[0]+lista1[1]+lista1[2])) + " naves")
    print("Jugador 2 inició con: " + str(cantidadNaves) + " naves")
    print("Jugador 2 derribó: " + str(cantidadNaves - (lista2[0]+lista2[1]+lista2[2])) + " naves")
    sys.exit()

def cambiarJugador(cont):
    '''Es el que se encarga de los turnos de los jugadores, osea es quien decide a quien le toca el turno.  '''
    if cont == 1:
        return 2
    else:
        return 1

def validarTiro(cont, fila, columna):
    '''Es el metodo que se encarga de decidir si el jugador 1 o 2 le dió o no a una nave. '''
    if cont == 1:
        if matriz1[fila][columna] == listaV[0]:
            lista1[0] = lista1[0] - 1
            matriz1[fila][columna] = 0
            print("Le has dado a un Avión")
        elif matriz1[fila][columna] == listaV[1]:
            lista1[1] = lista1[1] - 1
            matriz1[fila][columna] = 0
            print("Le has dado a un Tanque")
        elif matriz1[fila][columna] == listaV[2]:
            lista1[2] = lista1[2] - 1
            matriz1[fila][columna] = 0
            print("Le has dado a un Barco")
        else:
            print("Tiro fallido!")

    else:
        if matriz2[fila][columna] == 1:
            lista2[0] = lista2[0] - 1
            matriz2[fila][columna] = 0
            print("Le has dado a un Avión")
        elif matriz2[fila][columna] == 2:
            lista2[1] = lista2[1] - 1
            matriz2[fila][columna] = 0
            print("Le has dado a un Tanque")
        elif matriz2[fila][columna] == 3:
            lista2[2] = lista2[2] - 1
            matriz2[fila][columna] = 0
            print("Le has dado a un Barco")
        else:
            print("Tiro fallido!")

def validar80Porce(cont):
    '''Metodo para validar si un jugador ganó por el 80% más de naves que el otro.'''
    vehiculos2 = lista1[0] + lista1[1] + lista1[2]
    vehiculos1 = lista2[0] + lista2[1] + lista2[2]
    if vehiculos1 > vehiculos2:
        p1 = ((vehiculos1 - vehiculos2) / vehiculos1) * 100
        if p1>=80:
            print("Jugador 1 ganó")
            print("Razón: El jugador 2 tiene 80% menos de naves que el jugador 1")
            estadisticas()
    else:
        p2 = ((vehiculos2 - vehiculos1) / vehiculos2) * 100
        if p2>=80:
            print("Jugador 2 ganó")
            print("Razón: El jugador 1 tiene 80% menos de naves que el jugador 2")
            estadisticas()

def validar70Porce(cont):
    '''Metodo para validar si un jugador ganó por el 70% más de municiones que el otro.'''
    vehiculos2 = lista1[0] + lista1[1] + lista1[2]
    vehiculos1 = lista2[0] + lista2[1] + lista2[2]
    armas1 = (vehiculos1 * listaV[0].armas) - conta
    armas2 = (vehiculos2 * listaV[0].armas) - contb

    if armas1 > armas2:
        p1 = ((armas1 - armas2) / armas1) * 100
        if p1 >= 70:
            print("Jugador 1 ganó")
            print("Razón: El jugador 2 tiene 70% menos de armas que el jugador 1")
            estadisticas()

    else:
        p2 = ((armas2 - armas1) / armas2) * 100
        if p2 >= 70:
            print("Jugador 2 ganó")
            print("Razón: El jugador 1 tiene 70% menos de armas que el jugador 2")
            estadisticas()

def iniciarMatriz():
    '''Es el metodo que crea las matrises de ambos jugadores, con las cuales se jugara.'''
    for a in range(5):
        for b in range(5):
            num=rd.randint(0, 3)
            matriz1[a][b] = listaV[num]
            matriz2[b][a] = listaV[num]
    '''for aa in matriz1:
        print(aa)
    print(" ")
    for ae in matriz2:
        print(ae)'''

def conteoVehiculos1():
    '''Realiza el conteo de las naves del jugador 1.'''
    cont1 = 0
    cont2 = 0
    cont3 = 0
    for a in range(5):
        for b in range(5):
            if matriz1[a][b] == 1:
                cont1=cont1 + 1
                lista1[0]=cont1
            elif matriz1[a][b] == 2:
                cont2 = cont2 + 1
                lista1[1] = cont2
            else:
                if matriz1[a][b] == 3:
                    cont3 = cont3 + 1
                    lista1[2] = cont3

def conteoVehiculos2():
    '''Realiza el conteo de las naves del jugador 2.'''
    cont1 = 0
    cont2 = 0
    cont3 = 0
    for a in range(5):
        for b in range(5):
            if matriz2[a][b] == 1:
                cont1=cont1 + 1
                lista2[0]=cont1
            elif matriz2[a][b] == 2:
                cont2 = cont2 + 1
                lista2[1] = cont2
            else:
                if matriz2[a][b] == 3:
                    cont3 = cont3 + 1
                    lista2[2] = cont3

def agregarVehiculo():
    '''Este metodo se encarga de agregar las naves y municiones de los jugadores.'''
    nuevoVehiculo = Vehiculo("Avion",3)
    listaV.append(nuevoVehiculo)
    nuevoVehiculo = Vehiculo("Tanque",3)
    listaV.append(nuevoVehiculo)
    nuevoVehiculo = Vehiculo("Barco",3)
    listaV.append(nuevoVehiculo)
    nuevoVehiculo = Vehiculo("Vacio", 0)
    listaV.append(nuevoVehiculo)

def matrizGuia():
    '''Este metodo lo unico que hace es imprimir una matriz llena de 0 para que los jugadores se guien.'''
    matrizEjm = [[0 for n in range(5)] for m in range(5)]
    for e in range(5):
        for s in range(5):
            matrizEjm[e][s] = 0
    for z in matrizEjm:
        print(z)

def preguntaPosicion():
    '''Este metodo le indica al jugador que en que posición desea jugar.'''
    print("En cual posision quieres jugar : ")
    fila = int(input("Indica tu Fila : "))
    columna = int(input("Indica tu Columna : "))
    validarTiro(cont, fila, columna)

def documentacion():
    '''Se mostrará la documentación de todos los metodos.'''
    print(Vehiculo.__doc__)
    print(estadisticas.__doc__)
    print(cambiarJugador.__doc__)
    print(validarTiro.__doc__)
    print(validar80Porce.__doc__)
    print(validar70Porce.__doc__)
    print(iniciarMatriz.__doc__)
    print(conteoVehiculos1.__doc__)
    print(conteoVehiculos2.__doc__)
    print(agregarVehiculo.__doc__)
    print(matrizGuia.__doc__)
    print(preguntaPosicion.__doc__)
    print(documentacion.__doc__)


cont = 1
print("\tJuego Guerra de los Mundos\n Jugador:"+str(cont))
agregarVehiculo()
iniciarMatriz()
conteoVehiculos1()
conteoVehiculos2()
cantidadNaves= lista2[0]+lista2[1]+lista2[2]
opcion = input("Jugador "+str(cont)+ " desea realizar su turno??(s/n)").lower()
while opcion != 'n':
    if cont == 1:
        conta = conta + 1
    else:
        contb = contb + 1

    matrizGuia()
    preguntaPosicion()
    validar80Porce(cont)
    validar70Porce(cont)
    cont = cambiarJugador(cont)
    opcion = input("Jugador "+str(cont)+ " desea realizar su turno??(s/n)").lower()
else:
    if cont ==1:
        print("Jugador 2 ganó")
        print("Razón: El jugador 1 se retiró")
    else:
        print("Jugador 1 ganó")
        print("Razón: El jugador 2 se retiró")
    estadisticas()